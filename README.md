# Proyecto Ventilador controlado por temperatura 

## Descripción
El proyecto consiste en diseñar una PCB para un controlador de ventiladores AC, tanto el encendido como la velocidad de encendido se controlan mediante la variación en la temperatura de un componente llamado termistor. Asimismo, se tiene un componenete escencial que es tiristor el cuál permite la regulación del paso de corriente hacia el ventilador. Para la regulación de la corriente con el tiristor se utiliza una configuración de división de tensión, en donde se tiene primero un diodo Zener que permite regular la tensión a 18V, luego en un parte del circuito se tiene una tensión de refrencia de 12V, la cuál como su nombre lo dice es la referencia para la activación de los transistores que a su vez activan el tiristor. La variación de la tensión del divisor, se realiza mediante la variación de la resistencia en el termistor. Cuando la temperatura aumenta, la resistencia disminuye su valor, lo que genera un aumento en la tensión variable del divisor, tal que, si llega a ser mayor a los 12V de referencia, se activan los transistores y empieza el funcionamiento con el tiristor.

## Usos
Tener un ambiente controlado (fresco), sin necesidad de hacer nada más que tener el ventilador conectado y dejar que funcione por sí solo cuando se necesita.

## Tipo de instalación
La PCB se debe instalar en un encapsulado ya que debe ir dentro de un ventilador AC.

## Soporte
randolphvillegas1106@gmail.com

## Mapa de la ruta a seguir
Se diseñó el esquemático del circuito a elaborar en la PCB, así como la asignación de todos los parámetros de los componentes. Una vez verificado todo con respecto al espemático se procedió a montar el circuito en la placa, así como la definición de la forma del contorno de la placa. Con el trabajo del esquemático y el diseño de la placa, se mandó a imprimir la placa y a comprar los componentes para posteriormente proceder a soldar y verificar el funcionamiento de la PCB. 

### Implementaciones Futuras:
  * Implementación Futura 1: Poder controlar la velocidad de un motor DC de 12V dc mediante una orden a distancia usando infrarrojo.
  * Implementación Futura 2: Poder usar el controlador para un uso personal en un proyecto de mayor complejidad.

## Autores y reconocimiento
  * Brandon Palacio Delgado (brad.cr7@gmail.com)
  * Randolph Villegas Rodríguez (randolphvillegas1106@gmail.com)

## Fabricante
La fabricación de la PCB se realizará mediante la compañía de fabricación de PCBs: JLCPCB.
Las capacidades de fabricación de este fabricanete se encuentran en el siguiente enlace: https://jlcpcb.com/capabilities/pcb-capabilities

## Licencia
Este proyecto trabaja bajo la licencia GPLv3+ (GNU General Public License version 3 or later).

## Project status
Activo (2023.noviembre.18)
